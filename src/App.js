import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Home from "./components/Home/Home";
import Gallery from "./components/Gallery/Gallery";
import Contacts from "./components/Contacts/Contacts";

const App = () => {
  return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/contacts" component={Contacts}/>
          <Route path="/gallery" component={Gallery}/>
        </Switch>
      </BrowserRouter>
  );
};

export default App;
