import React from 'react';
import './Contacts.css';

const Contacts = props => {
    return (
        <div className="wrapper">
            <h2>Мои контакты</h2>
            <p>г. Москва, Тверская 7</p>
            <p><a href="tel:+7 (495) 123-45-67">+7 (495) 123-45-67</a></p>
            <p><a href="mailto:info@site.com">info@site.com</a></p>
            <button onClick={props.history.goBack} className="btn">Назад</button>
        </div>
    );
};

export default Contacts