import React from 'react';
import './Home.css';

const Home = () => {
    return (
        <>
            <div className="wrapper">
                <ul className="menu">
                    <li><a href="/">Home</a></li>
                    <li><a href={"gallery"}>Gallery</a></li>
                    <li><a href={"/contacts"}>Contacts</a></li>
                </ul>
            </div>
            <div className="wrapper">
                <h1 className="main">Добро пожаловать на мой сайт.</h1>
                <p className="main">
                    Меня зовут Ирина Амосова. Я профессиональный фотограф, вхожу в число лидеров Ассоциации детских и
                    семейных фотографов России, мои фотографии участвуют в выставках не только в России, но и в других
                    странах мира, мои работы входят в ТОП 100 лучших работ Ежегодной международной премии детских и
                    семейных фотографов BICF.
                    Как я пришла в фотографию?
                    С рождением моего сына я поняла, что хочу, что бы у него были красивые фотографии, а как
                    говорится..."Хочешь сделать хорошо, сделай это сам".....и понеслось. С 2011года я постоянно
                    фотографирую, можно сказать без перерыва. Художественное образование очень помогло и помогает мне в
                    создании моих работ. Занятия по живописи в школьные годы, после школы я получила специальность
                    "Дизайнер одежды", вообщем, сколько себя помню я всегда рисовала и видимо эта любовь к живописи
                    трансформировалась в мою любовь к фотографии. Естественно была отличная фотошкола Photoplay и
                    различные Мастер-классы фотографов.
                    Ах, да.....еще Финансовый Университет при Правительстве РФ по специальности "государственное и
                    муниципальное управление", не пригодилось, но и лишним не было)))))
                    Для меня это не работа, это мой образ жизни, то чем я живу и то что я люблю!
                    Основные направления съемки:
                    -семейная фотосессия
                    -детская фотосъемка
                    -портретная фотосессия
                    Я снимаю на профессиональное оборудование высокого уровня фирмы Canon. Светосильные объективы
                    позволяют мне снимать практически в любых условиях, поэтому помимо эмоциональных красивых кадров в
                    качестве фотографий вы также можете быть уверены. Съемки провожу в Москве и области, так же возможны
                    выезды в другие города России и Страны мира.
                    Если вы решили, что я буду вашим фотографом, можете не сомневаться, что фотографии которые вы
                    получите в итоге будут радовать вас и ваших близких долгие годы. </p>
            </div>
        </>
    );
};

export default Home;


