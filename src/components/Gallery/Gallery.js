import React from 'react';
import './Gallery.css';

const Gallary = props => {
       return (
        <>
            <input type="checkbox" id="pic-1"/>
            <label htmlFor="pic-1" className="lightbox"><img src="//unsplash.it/800/600?image=1" alt="image1"/></label>
            <input type="checkbox" id="pic-2"/>
            <label htmlFor="pic-2" className="lightbox"><img src="//unsplash.it/800/600?image=20" alt="image1"/></label>
            <input type="checkbox" id="pic-3"/>
            <label htmlFor="pic-3" className="lightbox"><img src="//unsplash.it/800/600?image=225" alt="image1"/></label>
            <input type="checkbox" id="pic-4"/>
            <label htmlFor="pic-4" className="lightbox"><img src="//unsplash.it/800/600?image=42" alt="image1"/></label>
            <input type="checkbox" id="pic-5"/>
            <label htmlFor="pic-5" className="lightbox"><img src="//unsplash.it/800/600?image=48" alt="image1"/></label>
            <input type="checkbox" id="pic-6"/>
            <label htmlFor="pic-6" className="lightbox"><img src="//unsplash.it/800/600?image=60" alt="image1"/></label>
            <input type="checkbox" id="pic-7"/>
            <label htmlFor="pic-7" className="lightbox"><img src="//unsplash.it/800/600?image=201" alt="image1"/></label>
            <input type="checkbox" id="pic-8"/>
            <label htmlFor="pic-8" className="lightbox"><img src="//unsplash.it/800/600?image=7" alt="image1"/></label>
            <input type="checkbox" id="pic-9"/>
            <label htmlFor="pic-9" className="lightbox"><img src="//unsplash.it/800/600?image=119" alt="image1"/></label>
            <input type="checkbox" id="pic-10"/>
            <label htmlFor="pic-10" className="lightbox"><img src="//unsplash.it/800/600?image=180" alt="image1"/></label>
            <input type="checkbox" id="pic-11"/>
            <label htmlFor="pic-11" className="lightbox"><img src="//unsplash.it/800/600?image=96" alt="image1"/></label>
            <input type="checkbox" id="pic-12"/>
            <label htmlFor="pic-12" className="lightbox"><img src="//unsplash.it/800/600?image=24" alt="image1"/></label>

            <div className="grid">
                <label htmlFor="pic-1" className="grid-item"><img src="//unsplash.it/400/300?image=1" alt="image1"/></label>
                <label htmlFor="pic-2" className="grid-item"><img src="//unsplash.it/400/300?image=20" alt="image2"/></label>
                <label htmlFor="pic-3" className="grid-item"><img src="//unsplash.it/400/300?image=225" alt="image3"/></label>
                <label htmlFor="pic-4" className="grid-item"><img src="//unsplash.it/400/300?image=42" alt="image4"/></label>
                <label htmlFor="pic-5" className="grid-item"><img src="//unsplash.it/400/300?image=48" alt="image5"/></label>
                <label htmlFor="pic-6" className="grid-item"><img src="//unsplash.it/400/300?image=60" alt="image6"/></label>
                <label htmlFor="pic-7" className="grid-item"><img src="//unsplash.it/400/300?image=201" alt="image7"/></label>
                <label htmlFor="pic-8" className="grid-item"><img src="//unsplash.it/400/300?image=7" alt="image8"/></label>
                <label htmlFor="pic-9" className="grid-item"><img src="//unsplash.it/400/300?image=119" alt="image9"/></label>
                <label htmlFor="pic-10" className="grid-item"><img src="//unsplash.it/400/300?image=180" alt="image10"/></label>
                <label htmlFor="pic-11" className="grid-item"><img src="//unsplash.it/400/300?image=96" alt="image11"/></label>
                <label htmlFor="pic-12" className="grid-item"><img src="//unsplash.it/400/300?image=24" alt="image12"/></label>
                <button  onClick={props.history.goBack} className="btn">Назад</button>
            </div>
        </>
    );
};

export default Gallary;
